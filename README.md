[![Debian Unstable](https://badges.debian.net/badges/debian/unstable/freedombox-desktop-client/version.svg)](https://packages.debian.org/unstable/freedombox-desktop-client)
[![Debian Testing](https://badges.debian.net/badges/debian/testing/freedombox-desktop-client/version.svg)](https://packages.debian.org/testing/freedombox-desktop-client)

# FreedomBox Desktop Client

Desktop client to detect local FreedomBox servers.

Written in Python 3 using QT.

# Development

Local build, install and run:

    $ dpkg-buildpackage -us -uc -b && sudo debi && freedombox-desktop-client
