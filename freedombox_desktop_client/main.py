#!/usr/bin/env python3

"""
.. module:: freedombox_desktop_client
   :synopsis: FreedomBox Desktop Client

"""

# Released under GPLv3+ license, see LICENSE

from argparse import ArgumentParser
from collections import namedtuple
import ipaddress
import json
import logging
import sys
import ssl
import urllib.request


from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QApplication

from setproctitle import setproctitle
from zeroconf import ServiceBrowser, Zeroconf

URL_TPL = "https://{}/plinth/api/1/shortcuts"
ZCFbx = namedtuple("ZCFbx", ["ipaddr", "name", "port"])


log = logging.getLogger(__name__)
window = None


def setup_logging(debug):
    ch = logging.StreamHandler()
    if debug:
        log.setLevel(logging.DEBUG)
        ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    ch.setFormatter(formatter)
    log.addHandler(ch)


def parse_args():
    ap = ArgumentParser()
    ap.add_argument('-c', '--cli', help='run from command line',
                    action='store_true')
    ap.add_argument('-d', '--debug', help='verbose logging',
                    action='store_true')
    args = ap.parse_args()
    return args


class BoxList(QtWidgets.QListWidget):
    """List of discovered boxes"""

    def clicked(self, item):
        text = item.text()
        try:
            box = window.known_fboxes[text]
        except KeyError:
            return  # race condition with removal

        window.current_lbl.setText(
            "Using: {}".format(text)
        )

        window.boxlist.hide()
        window.select_lbl.hide()

        window.backbutton.show()
        window.current_lbl.show()
        window.shortcut_list.show()

        self.shortcuts_loader_thread = ShortcutsLoaderThread(box.ipaddr)
        self.shortcuts_loader_thread.start()


    @QtCore.pyqtSlot(ZCFbx)
    def on_new_box_detected(self, f):
        desc = "{}:{}".format(f.ipaddr, f.port)
        if desc not in window.known_fboxes:
            window.known_fboxes[desc] = f
            i = QtWidgets.QListWidgetItem(desc)
            window.boxlist.addItem(i)


class ZCListener:

    def add_service(self, zeroconf, stype, name):
        info = zeroconf.get_service_info(stype, name)
        ipa = ipaddress.IPv4Address(info.address)
        box = ZCFbx(ipa, info.name, info.port)
        self._parent.newBoxDetected.emit(box)

    def remove_service(self, zeroconf, stype, name):
        # This is triggered only when a device actively announces its
        # unregistering :( As get_service_info cannot be used to fetch the
        # device ipaddr and port it's better to do nothing rather than
        # removing all devices with the same name
        pass


class ZeroConfListener(QtCore.QThread):

    newBoxDetected = pyqtSignal(ZCFbx)

    def run(self):
        self.newBoxDetected.connect(window.boxlist.on_new_box_detected)
        self._zeroconf = Zeroconf()
        listener = ZCListener()
        listener._parent = self
        ServiceBrowser(self._zeroconf, "_http._tcp.local.", listener)

    def on_quit(self):
        self._zeroconf.close()


def on_new_shortcut(desc):
    i = QtWidgets.QListWidgetItem(desc)
    window.shortcut_list.addItem(i)


def clear_shortcut_list():
    window.shortcut_list_model.removeRows(
        0,
        window.shortcut_list_model.rowCount()
    )


class ShortcutsLoaderThread(QtCore.QThread):

    def __init__(self, ipaddr):
        super(self.__class__, self).__init__()
        self.ipaddr = ipaddr

    def __del__(self):
        self.wait()

    @staticmethod
    def fetch_shortcuts(ipaddr):
        url = URL_TPL.format(ipaddr)
        ctx = ssl.SSLContext()
        # TODO: improve security
        ctx.verify_mode = ssl.CERT_NONE
        with urllib.request.urlopen(url, context=ctx) as resp:
            j = json.loads(resp.read())
        return j["shortcuts"]

    @staticmethod
    def fetch_icon(ipaddr, path):
        assert path.startswith("/")
        url = "https://{}{}".format(ipaddr, path)
        ctx = ssl.SSLContext()
        ctx.verify_mode = ssl.CERT_NONE
        with urllib.request.urlopen(url, context=ctx) as resp:
            return resp.read()

    newShortcutSignal = pyqtSignal(str)

    def run(self):
        clear_shortcut_list()
        shortcuts = self.fetch_shortcuts(self.ipaddr)

        self.newShortcutSignal.connect(on_new_shortcut)

        for s in shortcuts:
            if s.get("icon_url", None):
                icon = self.fetch_icon(self.ipaddr, s["icon_url"])
            else:
                icon = None
            desc = s["name"] + "\n" + s["short_description"]

            ic = QtGui.QIcon()
            px = QtGui.QPixmap()
            px.loadFromData(icon)
            ic.addPixmap(px)
            i = QtGui.QStandardItem(ic, desc)
            i.setEditable(False)
            window.shortcut_list_model.appendRow(i)


def on_back_click():
    window.backbutton.hide()
    window.current_lbl.hide()
    window.shortcut_list.hide()

    window.select_lbl.show()
    window.boxlist.show()
    clear_shortcut_list()


class MainWindow(QtWidgets.QWidget):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.known_fboxes = {}  # desc -> ZCFbx

        self.setWindowTitle("FreedomBox")
        self.setMinimumSize(600, 400)

        self.layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.layout)

        # box selection - shown at start

        self.select_lbl = QtWidgets.QLabel("Select a FreedomBox", self)
        self.select_lbl.setAlignment(QtCore.Qt.AlignCenter)
        self.layout.addWidget(self.select_lbl)

        self.boxlist = BoxList()
        self.boxlist.itemClicked.connect(self.boxlist.clicked)
        # TODO handle Enter
        self.boxlist.show()
        self.layout.addWidget(self.boxlist)

        # box applications list - hidden at start

        self.backbutton = QtWidgets.QPushButton('<- back', self)
        self.backbutton.hide()
        self.backbutton.clicked.connect(on_back_click)
        self.layout.addWidget(self.backbutton)

        self.current_lbl = QtWidgets.QLabel("", self)
        self.current_lbl.setAlignment(QtCore.Qt.AlignCenter)
        self.current_lbl.hide()
        self.layout.addWidget(self.current_lbl)

        self.shortcut_list = QtWidgets.QTreeView()
        self.shortcut_list.setRootIsDecorated(False)
        self.shortcut_list.setAlternatingRowColors(False)
        self.shortcut_list.setSortingEnabled(True)
        self.shortcut_list.setAnimated(True)
        self.shortcut_list.NoEditTriggers=True
        self.shortcut_list_model = QtGui.QStandardItemModel()
        self.shortcut_list.setModel(self.shortcut_list_model)

        self.shortcut_list.hide()
        self.layout.addWidget(self.shortcut_list)

        self.show()




def main():
    global window

    setproctitle('freedombox-desktop-client')
    args = parse_args()
    setup_logging(args.debug)

    app = QApplication([])
    window = MainWindow()

    zcl = ZeroConfListener()
    app.aboutToQuit.connect(zcl.on_quit)
    zcl.start()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
